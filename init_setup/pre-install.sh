#!/usr/bin/env bash

#******************************
# ** USERS
#******************************

echo '*** adding root password:'
sudo passwd

echo '*** adding www user:'
adduser www

echo '*** adding www2 user:'
adduser www2

echo '*** adding www user to sudoers:'
sudo adduser www sudo

echo '*** adding www2 user to sudoers:'
sudo adduser www2 sudo

echo '*** create ssh-key for www user:'
sudo -H -u www bash -c 'ssh-keygen -b 4096 -t rsa'

echo '*** create ssh-key for www2 user:'
sudo -H -u www2 bash -c 'ssh-keygen -b 4096 -t rsa'

#******************************
# ** PACKAGES
#******************************

echo '*** apg-get update'
apg-get update

echo '*** apt-get'
apt-get install python3-tz python3-pip vim htop collectl screen tmux default-jre stunnel4 build-essential python3-setuptools python-setuptools python-dev python3-dev nginx mysql-server mysql-common mysql-client python-mysqldb s3cmd xvfb language-pack-sr tcl8.5 openssh-server curl python-dateutil python-tz libmysqlclient-dev build-essential libgl1-mesa-dev build-essential libglu1-mesa-dev -y qt5-default qttools5-dev-tools libqt5webkit5 libqtwebkit-qmlwebkitplugin libqt5webkit5-dev python-pycurl

echo '*** easy_install'
easy_install tornado sqlalchemy boto psutil alembic mandrill pyDNS validate_email xmltodict

echo '*** easy_install3'
easy_install3 sqlalchemy tornado pymysql redis

echo '*** apg-get upgrade'
sudo apt-get upgrade

chmod g-wx,o-wx ~/.python-eggs

#******************************
# ** RECONFIGURATION
#******************************

# ** mysql
mkdir /var/log/mysql-binary-log/
chown mysql.mysql /var/log/mysql-binary-log
# ** add users
mysql -uroot -e "create user 'sbp'@'localhost' identified by 'pr3mi3r';"
mysql -uroot -e "create user 'sbp'@'%' identified by 'pr3mi3r';"
mysql -uroot -e "grant all on *.* to 'sbp'@'localhost';"
mysql -uroot -e "grant all on *.* to 'sbp'@'%';"
mysql -uroot -e "create user 'sbpread'@'localhost' identified by 'pr3mi3r';"
mysql -uroot -e "create user 'sbpread'@'%' identified by 'pr3mi3r';"
mysql -uroot -e "grant select on *.* to 'sbpread'@'localhost';"
mysql -uroot -e "grant select on *.* to 'sbpread'@'%';"
service mysql restart

# ** localization
locale-gen sr_RS@latin
dpkg-reconfigure locales

# ** redis
cd /opt
git clone https://github.com/antirez/redis.git
cd redis
make -j 16
make install
easy_install redis

cd /opt
# ** node.js
git clone https://github.com/nodejs/node
cd node
./configure
make -j 12
make install

# ** pyqt5 sip
cd /opt
wget http://sourceforge.net/projects/pyqt/files/sip/sip-4.18/sip-4.18.tar.gz
tar zxvf sip-4.18.tar.gz
cd sip-4.18
python3 configure.py
make -j 12
make install

# ** PyQt5 - 5.6
cd /opt
wget http://downloads.sourceforge.net/project/pyqt/PyQt5/PyQt-5.6/PyQt5_gpl-5.6.tar.gz
tar zxvf PyQt5_gpl-5.6.tar.gz
cd PyQt5_gpl-5.6
python3 configure.py
make  -j 12
make install

# ** SWAP allocation 4GB
fallocate -l 4G /swapfile
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile

echo 'Done. Insert ssh key pairs from www and www2 to git-hub account and than run post-install.sh'
