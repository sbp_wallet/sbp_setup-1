#!/usr/bin/env bash

# ***************************************************************************
# *************************** POST INSTALL *****************************
# ***************************************************************************

# ** dirs setup
mkdir /etc/SportBettingPlatform/config -p
mkdir /var/log/sbp/actions -p
mkdir /var/log/sbp/ttx -p
mkdir /var/log/sbp2 -p
mkdir /data/www -p
mkdir /data/registar -p
mkdir /data/www-site -p
mkdir /data/www-s3/sbp/rev -p
mkdir /data/www/static -p
mkdir /home/www/dev-soft
mkdir /home/www2/dev-soft

chown www.www /var/log/sbp -R
chown www2.www2 /var/log/sbp2 -R
chown www.www /etc/SportBettingPlatform/config -R
chown www.www /data -R

chmod 0755 /var/log/sbp -R
chmod 0755 /var/log/sbp2 -R
chmod 0755 /etc/SportBettingPlatform/config -R
chmod 0755 /data -R

# ** clone sbp 1 projects from git
sudo -H -u www bash -c 'mkdir /home/www/work'
sudo -H -u www bash -c 'cd /home/www/work && git clone git@github.com:sportbettingplatform/sbp.git sportbettingplatform'
sudo -H -u www bash -c 'cd /home/www/work && git clone git@github.com:sportbettingplatform/frontend_core.git'
sudo -H -u www bash -c 'cd /home/www/work && git clone git@github.com:sportbettingplatform/sbp_www.git sportbettingplatform_www'
sudo -H -u www bash -c 'cd /home/www/work && git clone git@github.com:sportbettingplatform/kiosk.git sportbettingplatform_kiosk'
sudo -H -u www bash -c 'cd /home/www/work && git clone git@github.com:sportbettingplatform/mobile.git'
sudo -H -u www bash -c 'cd /home/www/work && git clone git@github.com:sportbettingplatform/games.git'

# ** clone sbp 2 projects from git
sudo -H -u www2 bash -c 'mkdir /home/www2/work'
sudo -H -u www2 bash -c 'cd /home/www2/work && git clone git@github.com:sportbettingplatform/sbp2.git'
sudo -H -u www2 bash -c 'cd /home/www2/work && git clone git@github.com:sportbettingplatform/www2.git'
sudo -H -u www2 bash -c 'cd /home/www2//work && git clone git@github.com:sportbettingplatform/kiosk2.git'

# ** link projects files
cd /usr/local/bin
ln -sf /home/www/work/sportbettingplatform/src/scripts/clearAnouncedPayouts.py clearAnouncedPayouts
ln -sf /home/www/work/sportbettingplatform/src/scripts/copyBinaryLogsToDropbox.py copyBinaryLogsToDropbox.py
ln -sf /home/www/work/sportbettingplatform/src/scripts/onlineParserCtl.py onlineParserCtl
ln -sf /home/www/work/sportbettingplatform/src/scripts/onlineParserCtl.py onlineParserCtl.py
ln -sf /home/www/work/sportbettingplatform/src/scripts/portChecker.py portChecker
ln -sf /home/www/work/sportbettingplatform/src/scripts/sendLogFiles.py sendLogFiles.py
ln -sf /home/www2/work/sbp2/src/scripts/startServices.py startSbp2Services
ln -sf /home/www/work/sportbettingplatform/src/scripts/startSbpServices.py startSbpServices
ln -sf /home/www/work/sportbettingplatform/src/scripts/stopSbpServices.sh
ln -sf /home/www/work/sportbettingplatform/src/scripts/clearTmpDir.sh
ln -sf /home/www/work/sportbettingplatform/src/scripts/clearLogs.sh
ln -sf /home/www/work/sportbettingplatform/src/scripts/dumpSbpDatabases.sh
ln -sf /home/www/work/sportbettingplatform/src/scripts/live_increaseRound.sh
ln -sf /home/www/sbp_setup/env_setup/init.sh pr-env-init.sh
ln -sf /home/www/sbp_setup/env_setup/atlanticnet.sh pr-atl-init.sh
ln -sf /home/www/sbp_setup/env_setup/production.sh pr-prod-init.sh
ln -sf /home/www/sbp_setup/env_setup/staging.sh pr-stg-init.sh
ln -sf /home/www/sbp_setup/env_setup/local.sh pr-local-ini.sh

# ** setup admin / liveboard
cd /data/www
ln -sf /home/www/work/sportbettingplatform_www/appdirectory-build
ln -sf /home/www/work/sportbettingplatform_www/audio
ln -sf /home/www/work/sportbettingplatform_www/css
ln -sf /home/www/work/sportbettingplatform_www/imgs
ln -sf /home/www/work/sportbettingplatform_www/js
ln -sf /home/www/work/sportbettingplatform_www/pages
ln -sf /home/www/work/sportbettingplatform_www/admin-dev.html dev.html
ln -sf /home/www/work/sportbettingplatform_www/admin-prod.html prod.html
ln -sf /home/www/work/sportbettingplatform_www/templates
ln -sf /home/www/work/sportbettingplatform_www/liveboard-dev.html
ln -sf /home/www/work/sportbettingplatform_www/liveboard-prod.html
ln -sf /home/www/work/sportbettingplatform_www/liveboard-dev.html liveboard-redesign-dev.html
ln -sf /home/www/work/sportbettingplatform_www/liveboard-prod.html liveboard-redesign-prod.html
ln -sf dev.html index.html
cd static
ln -sf /data/www-s3/sbp/rev

# ** setup web
cd /data/www-site
ln -sf /home/www/work/sportbettingplatform_www/admin-dev.html
ln -sf /home/www/work/sportbettingplatform_www/admin-prod.html
ln -sf /home/www/work/sportbettingplatform_www/appdirectory-build
ln -sf /home/www/work/sportbettingplatform_www/audio
ln -sf /home/www/work/sportbettingplatform_www/css
ln -sf /home/www/work/sportbettingplatform_www/imgs
ln -sf /home/www/work/sportbettingplatform_www/js
ln -sf /home/www/work/sportbettingplatform_www/site-dev.html
ln -sf /home/www/work/sportbettingplatform_www/site-prod.html
ln -sf /home/www/work/sportbettingplatform_www/templates
ln -sf /home/www/work/sportbettingplatform_www/static
ln -sf site-dev.html index.html
cd static
ln -sf /data/www-s3/sbp/rev

# ** setup static files dir
cd /data/www/static
ln -sf ../../www-site/static/css
ln -sf ../../www-site/static/fonts
ln -sf ../../www-site/static/images
ln -sf /data/www-s3/sbp/rev

# ** bind frontend-core
cd /data/www-site/js && ln -sf ../../frontend_core/js/core core

# ** register
cd /data/registar
ln -sf /home/www/work/sportbettingplatform_www/appdirectory-build
ln -sf /home/www/work/sportbettingplatform_www/audio
ln -sf /home/www/work/sportbettingplatform_www/css
ln -sf /home/www/work/sportbettingplatform_www/imgs
ln -sf /home/www/work/sportbettingplatform_www/js
ln -sf /home/www/work/sportbettingplatform_www/register-dev.html
ln -sf /home/www/work/sportbettingplatform_www/register-prod.html
ln -sf /home/www/work/sportbettingplatform_www/templates
ln -sf ../www/static

# ** setup pythonpath
sudo -H -u www bash -c 'echo "\n\n" >> ~/.bashrc'
sudo -H -u www bash -c 'echo "export PYTHONPATH="/etc/SportBettingPlatform:/home/www/work/sportbettingplatform/src:\${PYTHONPATH}"" >> ~/.bashrc'

sudo -H -u www2 bash -c 'echo "\n\n" >> ~/.bashrc'
sudo -H -u www2 bash -c 'echo "export PYTHONPATH="/home/www2/work/sbp2/src:\${PYTHONPATH}"" >> ~/.bashrc'

# ** add hosts
echo -e "127.0.0.1 admin.sbp.dev\tadmin" >> /etc/hosts
echo -e "127.0.0.1 kiosk.sbp.dev\tkiosk" >> /etc/hosts
echo -e "127.0.0.1 web.sbp.dev\tweb" >> /etc/hosts
echo -e "127.0.0.1 reg.sbp.dev\treg" >> /etc/hosts
echo -e "127.0.0.1 bingo.sbp.dev\tbingo" >> /etc/hosts
echo -e "127.0.0.1 m.sbp.dev\tmobile" >> /etc/hosts
echo -e "127.0.0.1 live.sbp.dev\tlive" >> /etc/hosts
echo -e "127.0.0.1 live2.sbp.dev\tlive2" >> /etc/hosts

# ** setup main nginx file
cp ~/sbp_setup/init_setup/configs/configs/nginx-conf/nginx.conf /etc/nginx

# ** bind config files
tar zxvf ~/sbp_setup/init_setup/configs/etc_sportbetting_platform.tgz -C /etc/SportBettingPlatform/

# ** setup upstreams
tar zxvf ~/sbp_setup/init_setup/configs/nginx-conf/conf_d.tgz -C /etc/nginx/conf.d

# ** setup nginx hosts
tar zxvf ~/sbp_setup/init_setup/configs/nginx-conf/hosts.tgz -C /etc/nginx/sites-available

# ** remove default servers
rm /etc/nginx/sites-enabled/default /etc/nginx/sites-available/default

# ** setup projects servers
cd /etc/nginx/sites-enabled
ln -sf ../sites-available/bingo_new
ln -sf ../sites-available/kiosk
ln -sf ../sites-available/kladionica
ln -sf ../sites-available/liveboard
ln -sf ../sites-available/liveboard_new
ln -sf ../sites-available/mobile
ln -sf ../sites-available/premierbetme
ln -sf ../sites-available/registar
ln -sf ../sites-available/static

service nginx restart

tar zxvf ~/sbp_setup/init_setup/dev-soft/bash-git-prompt.tgz -C /home/www/dev-soft
sudo -H -u www bash -c 'echo "\n\n" >> ~/.bashrc'
sudo -H -u www bash -c 'GIT_PROMPT_ONLY_IN_REPO=1" >> ~/.bashrc'
sudo -H -u www bash -c 'echo "\n\n" >> ~/.bashrc'
sudo -H -u www bash -c 'source ~/dev-soft/bash-git-prompt/gitprompt.sh" >> ~/.bashrc'

# ** download pycharm
cd /home/www/dev-soft
wget 'https://download.jetbrains.com/python/pycharm-professional-2016.1.2.tar.gz'

# ** download web storm
wget 'https://download.jetbrains.com/webstorm/WebStorm-2016.1.1.tar.gz'

# ** download sublime
wget 'https://download.sublimetext.com/sublime-text_build-3103_amd64.deb'

# ** copy IDE's to another user
cp -R /home/www/dev-soft/* /home/www2/dev-soft
chown www2.www2 /home/www2/dev-soft -R

# ** env setup commands
cp -R ~/sbp_setup/env_setup/* /home/www/sbp_setup/env_setup

echo "*************************************"
echo "licence server for jetbrains IDE\'s:"
echo "http://idea.qinxi1992.cn"
echo "*************************************"

# ** create databases:
mysql -usbp -ppr3mi3r -e "create database live"
mysql -usbp -ppr3mi3r -e "create database events"
mysql -usbp -ppr3mi3r -e "create database master"
mysql -usbp -ppr3mi3r -e "create database logs"
mysql -usbp -ppr3mi3r -e "create database reports"
mysql -usbp -ppr3mi3r -e "create database sbp2bingo"
mysql -usbp -ppr3mi3r -e "create database sbp2master"
mysql -usbp -ppr3mi3r -e "create database sbp2service4700"
mysql -usbp -ppr3mi3r -e "create database sbp2transactions"
mysql -usbp -ppr3mi3r -e "create database atickets"
mysql -usbp -ppr3mi3r -e "create database tickets4600"
mysql -usbp -ppr3mi3r -e "create database tickets4601"
mysql -usbp -ppr3mi3r -e "create database tickets4602"
mysql -usbp -ppr3mi3r -e "create database tickets4603"
mysql -usbp -ppr3mi3r -e "create database tickets4604"
mysql -usbp -ppr3mi3r -e "create database tickets4605"
mysql -usbp -ppr3mi3r -e "create database tickets4606"
mysql -usbp -ppr3mi3r -e "create database tickets4607"
mysql -usbp -ppr3mi3r -e "create database tickets4608"
mysql -usbp -ppr3mi3r -e "create database tickets4609"
mysql -usbp -ppr3mi3r -e "create database tickets5600"
mysql -usbp -ppr3mi3r -e "create database tickets5601"
mysql -usbp -ppr3mi3r -e "create database tickets5602"
mysql -usbp -ppr3mi3r -e "create database tickets5603"
mysql -usbp -ppr3mi3r -e "create database tickets5604"
mysql -usbp -ppr3mi3r -e "create database tickets5605"
mysql -usbp -ppr3mi3r -e "create database tickets5606"
mysql -usbp -ppr3mi3r -e "create database tickets5607"
mysql -usbp -ppr3mi3r -e "create database tickets5608"
mysql -usbp -ppr3mi3r -e "create database tickets5609"
mysql -usbp -ppr3mi3r -e "create database tickets9998"
mysql -usbp -ppr3mi3r -e "create database tickets9999"
mysql -usbp -ppr3mi3r -e "create database users4590"
mysql -usbp -ppr3mi3r -e "create database users4591"


echo "Done. Download latest database from aws"
echo "add hostname to /home/www/work/sportbettingplatform/src/config.sample/live.py.sample"
echo "add hostname to /home/www/work/sportbettingplatform/src/config.sample/events.py.sample"
echo "add hostname to /home/www/work/sportbettingplatform/src/scripts/startBalancingServices.sh"
echo "add hostname to /home/www/work/sportbettingplatform_www/setFrontendSettings.py"

